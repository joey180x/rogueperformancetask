﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour {


[HideInInspector] public bool facingRight = true;
[HideInInspector] public bool jump = true;
public float moveForce = 365f;
public float maxSpeed= 5f;
public float jumpForce = 100f;
public int movementspeed = 10;
	private float moveSpeed = 10f;



	private Rigidbody2D rb2d;
// Use this for initialization
void Awake () {
	rb2d = GetComponent<Rigidbody2D> ();
}


	
	// Update is called once per frame
void Update () {

		if (Input.GetKey (KeyCode.A)) {
			transform.Translate (Vector3.left * movementspeed * Time.deltaTime); 
		}
		if (Input.GetKey (KeyCode.D)) {
			transform.Translate (Vector3.right * movementspeed * Time.deltaTime);
			  
		}

	
		if (Input.GetKey (KeyCode.W)) {

			transform.Translate (Vector3.up * movementspeed * Time.deltaTime);
			jump = true;



		}


	}
	void FixedUpdate(){
		
		float h = Input.GetAxis ("Horizontal");
		//
		if (h > 0 && !facingRight)
			Flip ();
		else if (h < 0 && facingRight)
			Flip ();
		
		
		
		
	}
	void Flip(){
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}
	


}